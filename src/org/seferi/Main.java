package org.seferi;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {
    static char[][] allSeats;
    static int cinemaRows;
    static int cinemaSeats;
    static int purchasedTickets = 0;
    static float purchasedTicketsAsPercentage;
    static int currentIncome;
    static int totalIncome;


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of rows:");
        cinemaRows = scanner.nextInt();
        System.out.println("Enter the number of seats in each row:");
        cinemaSeats = scanner.nextInt();
        allSeats = new char[cinemaRows][cinemaSeats];

        for (int i = 0; i < cinemaRows; i++) {
            for (int j = 0; j < cinemaSeats; j++) {
                allSeats[i][j] = 'S';
            }
        }

        while (true) {
            System.out.println("\n1. Show the seats\n" +
                    "2. Buy a ticket\n" +
                    "3. Statistics\n" +
                    "0. Exit");
            switch (scanner.nextInt()) {
                case 1:
                    showSeats();
                    break;
                case 2:
                    getSeat();
                    break;
                case 3:
                    statistics();
                    break;
                case 0:
                    return;
            }
        }

    }

    public static void statistics() {
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_EVEN);

        // Show purchased tickets as percentage
        int totalSeats = cinemaRows * cinemaSeats;
        purchasedTicketsAsPercentage = ((purchasedTickets * 100) / (float) totalSeats);

        System.out.printf("\nNumber of purchased tickets: %d", purchasedTickets);
        System.out.println("\nPercentage: " + df.format(purchasedTicketsAsPercentage) + "%");
        System.out.println("Current Income: $" + currentIncome);
        getTotalIncome();
        System.out.println();

    }

    private static void showSeats() {
        int count = 1;
        System.out.println();
        System.out.print("Cinema:\n");
        System.out.print(" ");
        for (int i = 1; i <= cinemaSeats; i++) {
            System.out.print(" " + i);
        }
        System.out.println();
        for (int i = 0; i < allSeats.length; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < allSeats[i].length; j++) {
                System.out.print(" " + allSeats[i][j]);
            }
            System.out.println();
        }


    }

    public static void getTotalIncome() {

        int totalSeats = cinemaRows * cinemaSeats;

        if (totalSeats <= 60) {
            totalIncome = totalSeats * 10;
        } else {
            int frontHalf = cinemaRows / 2;
            int backHalf = cinemaRows - frontHalf;
            totalIncome = (frontHalf * cinemaSeats) * 10 + (backHalf * cinemaSeats) * 8;
        }
        System.out.printf("Total income:$%d", totalIncome);
    }

    public static void getSeat() {
        Scanner scanner = new Scanner(System.in);
        int ticketPrice;
        int ticketRow;
        int ticketSeat;
        while (true) {
            System.out.println("\nEnter a row number:");
            ticketRow = scanner.nextInt();
            System.out.println("Enter a seat number in that row:");
            ticketSeat = scanner.nextInt();

            if (ticketRow <= 0 || ticketRow > cinemaRows || ticketSeat <= 0 || ticketSeat > cinemaSeats) {
                System.out.println("Wrong input!");
                continue;
            } else if (allSeats[ticketRow - 1][ticketSeat - 1] == 'B') {
                System.out.println("That ticket has already been purchased!");
                continue;
            }
            if (cinemaRows * cinemaSeats <= 60 || ticketRow <= cinemaRows / 2) {
                ticketPrice = 10;
                break;
            } else {
                ticketPrice = 8;
                break;
            }
        }

        currentIncome += ticketPrice;
        purchasedTickets++;
        System.out.printf("\nTicket price: $%d%n", ticketPrice);

        allSeats[ticketRow - 1][ticketSeat - 1] = 'B';
    }

}
